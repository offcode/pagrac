package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"

	"github.com/machinebox/graphql"
	"github.com/urfave/cli/v2"
)

type SimpleQuery struct {
	client *graphql.Client
	query string
	writer io.StringWriter
	token string
	vars map[string]interface{}
}

type PaginatedQuery struct {
	maxPages      int
	simpleQuery   *SimpleQuery
	pagesPath     []string
	pageInfoProp  string
	hasNextProp   string
	cursorProp    string
	cursorVarName string
	nodesProp     string
}

func newQuery(url string, query string) *SimpleQuery {
	return &SimpleQuery{
		client: graphql.NewClient(url),
		query:  query,
		writer: os.Stdout,
		token:  "",
		vars:   make(map[string]interface{}),
	}
}

func newPaginatedQuery(url string, query string, pagesPath []string) *PaginatedQuery {
	simple := newQuery(url, query)
	return &PaginatedQuery{
		maxPages:      0,
		simpleQuery:   simple,
		pagesPath:     pagesPath,
		pageInfoProp:  "pageInfo",
		hasNextProp:   "hasNextPage",
		cursorProp:    "endCursor",
		cursorVarName: "after",
		nodesProp:     "nodes",
	}
}

func (q *SimpleQuery) execOnce(response interface{}) (error) {
	request := graphql.NewRequest(q.query)
	if q.token != "" {
		request.Header.Set("Authorization", fmt.Sprintf("bearer %v", q.token))
	}
	for k, v := range q.vars {
		request.Var(k, v)
	}
	if err := q.client.Run(context.Background(), request, &response); err != nil {
		return err
	}
	return nil
}

func (q *SimpleQuery) execToJson() (error) {
	var response interface{}
	if err := q.execOnce(&response); err != nil {
		return err
	}
	return toJson(response, q.writer)
}

func toJson(o interface{}, writer io.StringWriter) (error) {
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(o); err != nil {
		return err
	}
	_, err := writer.WriteString(buf.String())
	return err
}

func (pq *PaginatedQuery) execPaginated() (error) {
	for round := 0; round < pq.maxPages; round++ {
		var response interface{}
		if err := pq.simpleQuery.execOnce(&response); err != nil {
			return err
		}
		parent := getProp(response, pq.pagesPath)
		pageInfo := getProp(parent, []string{pq.pageInfoProp})
		var nodes []interface{}
		rawNodes := getProp(parent, []string{pq.nodesProp})
		if rawNodes != nil {
			nodes = rawNodes.([]interface{})
		}
		//fmt.Printf("resp: %v\nparent: %v\npageInfo: %v\nraw: %v\nnodes: %v\n", response, parent, pageInfo, rawNodes, nodes)
		for _, node := range nodes {
			toJson(node, pq.simpleQuery.writer)
		}
		hasNext := getProp(pageInfo, []string{pq.hasNextProp})
		if fmt.Sprintf("%v", hasNext) != "true" {
			break
		}
		cursorValue := getProp(pageInfo, []string{pq.cursorProp})
		pq.simpleQuery.vars[pq.cursorVarName] = fmt.Sprintf("%v", cursorValue)
	}
	return nil
}

func getProp(obj interface{}, path []string) interface{} {
	if obj == nil {
		return nil
	}
	switch len(path) {
	case 0:
		return obj
	case 1:
		return obj.(map[string]interface{})[path[0]]
	default:
		deeper := obj.(map[string]interface{})[path[0]]
		return getProp(deeper, path[1:])
	}
}

func mainCmd(c *cli.Context) error {
	// query
	query := c.String("query")
	if query == "" {
		from := c.String("from")
		data, err := ioutil.ReadFile(from)
		if err != nil {
			return err
		}
		query = string(data)
	}
	q := newQuery(c.String("url"), query)

	// vars
	for _, v := range c.StringSlice("var") {
		kv := strings.Split(v, "=")
		i, err := strconv.Atoi(kv[1])
		if err == nil {
			q.vars[kv[0]] = i
		} else {
			q.vars[kv[0]] = kv[1]
		}
	}

	// token
	tokenVar := c.String("token-env-var")
	if tokenVar != "" {
		q.token = os.Getenv(tokenVar)
	}

	// output
	if toFile := c.String("to"); toFile != "" {
		var err error
		var f *os.File
		if c.Bool("append") {
			f, err = os.OpenFile(toFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		} else {
			f, err = os.Create(toFile)
		}
		defer f.Close()
		if err != nil {
			return err
		}
		q.writer = f
	}

	// pagination
	maxPages := c.Int("max-pages")
	if maxPages <= 0 {
		maxPages = 10000
	}
	pq := PaginatedQuery{
		maxPages:      maxPages,
		simpleQuery:   q,
		pagesPath:     strings.Split(c.String("pages-path"), "."),
		pageInfoProp:  c.String("page-info-prop"),
		hasNextProp:   c.String("has-next-prop"),
		cursorProp:    c.String("cursor-prop"),
		cursorVarName: c.String("cursor-var-name"),
		nodesProp:     c.String("nodes-prop"),
	}

	return pq.execPaginated()
}

func makeApp() *cli.App {
	dummy := newPaginatedQuery("_", "_", nil)
	app := &cli.App{
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name: "url",
				Value: "https://api.github.com/graphql",
				Usage: "GraphQL endpoint",
				Required: true,
			},
			&cli.StringFlag{
				Name: "query",
				Aliases: []string{"q"},
				Usage: "GraphQL query string",
			},
			&cli.StringFlag{
				Name: "from",
				Aliases: []string{"f"},
				Usage: "`FILE` contains a GraphQL query",
			},
			&cli.StringFlag{
				Name: "to",
				DefaultText: "stdout",
				Usage: "Results are written to `FILE`",
			},
			&cli.BoolFlag{
				Name:        "append",
				Aliases:     []string{"a"},
				Usage:       "Append results to file specified by --to",
				Value:       false,
			},
			&cli.StringFlag{
				Name: "token-env-var",
				Aliases: []string{"t"},
				Usage: "Name of the env var that holds the auth token value",
			},
			&cli.StringSliceFlag{
				Name: "var",
				Aliases: []string{"v"},
				Usage: "Variable of the form key=value (you can have many of them)",
			},
			&cli.StringFlag{
				Name: "cursor-prop",
				Value: dummy.cursorProp,
				Usage: "The key in pageInfo that holds the cursor",
			},
			&cli.StringFlag{
				Name: "has-next-prop",
				Value: dummy.hasNextProp,
				Usage: "The key in pageInfo that shows if there is a next page",
			},
			&cli.StringFlag{
				Name: "cursor-var-name",
				Value: dummy.cursorVarName,
				Usage: "Name of variable in the graphql query that will hold the startCursor for the next page",
			},
			&cli.StringFlag{
				Name: "pages-path",
				Usage: "Dot-separated path in the result json that leads to the PageInfo object (without the data node)",
			},
			&cli.StringFlag{
				Name: "page-info-prop",
				Value: dummy.pageInfoProp,
				Usage: "The pageInfo key",
			},
			&cli.StringFlag{
				Name: "nodes-prop",
				Value: dummy.nodesProp,
				Usage: "The key that leads to the list we want to iterate over",
			},
			&cli.IntFlag{
				Name: "max-pages",
				Value: 0,
				DefaultText: "Infinity",
				Usage: "Maximum number of pages to request. A negative or zero value means, request all pages",
			},
		},
		Action: mainCmd,
	}
	return app
}

func main() {
	err := makeApp().Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}

}
