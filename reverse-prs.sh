go run pagrac.go -t GH_TOKEN \
	--url https://api.github.com/graphql \
	--from test_queries/reverse-pull-requests.graphql \
	--append \
	--var perPage=100 \
	--pages-path 'search' \
	--nodes-prop 'edges' \
	--to pull-requests.jsonl
