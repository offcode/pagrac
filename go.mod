module gitlab.com/offcode/pagrac

go 1.13

require (
	github.com/machinebox/graphql v0.2.2
	github.com/matryer/is v1.3.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/urfave/cli/v2 v2.2.0
)
