go run pagrac.go -t GH_TOKEN \
	--url https://api.github.com/graphql \
	--from test_queries/pull-requests.graphql \
	--var perPage=2 \
	--max-pages 2 \
	--pages-path 'repository.pullRequests'
