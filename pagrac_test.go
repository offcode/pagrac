package main

import (
	"os"
	"strings"
	"testing"
)

func TestQueries(t *testing.T) {
	type QueryTest struct {
		query string
		vars map[string]interface{}
		result string
	}
	tests := []QueryTest {
		{
			query:
				`{
					pokemons(first:1) {
						name
					}
				}`,
			vars:   nil,
			result: `{"pokemons":[{"name":"Bulbasaur"}]}`,
		},
		{
			query : `
				query($id:String!, $name:String!){
					pokemon(id:$id, name:$name) {
						classification
					}
				}`,
			vars: map[string]interface{} {
				"id": "UG9rZW1vbjowMDE=",
				"name": "Bulbasaur",
			},
			result: `{"pokemon":{"classification":"Seed Pokémon"}}`,
		},
	}
	for _, qt := range tests {
		url := "https://graphql-pokemon.now.sh/"
		q := newQuery(url, qt.query)
		q.vars = qt.vars
		var b strings.Builder
		q.writer = &b
		if err := q.execToJson(); err != nil {
			t.Fatal(err)
		}
		got := strings.TrimSpace(b.String())
		if got != qt.result {
			t.Fatalf("For query: %v\nExpected: %v\nGot: %v", qt.query, qt.result, got)
		}
	}
}

func TestGitlabAuth(t *testing.T) {
	url := "https://gitlab.com/api/graphql"
	query := `
		query {
		  currentUser {
			username
		  }
		}
	`
	simple := newQuery(url, query)
	simple.token = os.Getenv("PAGRAC_GITLAB")
	simple.execToJson()
}

func TestGithubAuth(t *testing.T) {
	url := "https://api.github.com/graphql"
	query := `
		query {
		  viewer {
			login
		  }
		}
	`
	simple := newQuery(url, query)
	simple.token = os.Getenv("GH_TOKEN")
	simple.execToJson()
}

func TestPaginatedQuery(t *testing.T) {
	url := "https://api.github.com/graphql"
	query := `
		query($after:String) { 
		  repository(owner:"ethereum", name:"go-ethereum") {
			issues(first:2, after:$after) {
			  pageInfo {
				endCursor
				hasNextPage
			  }
			  nodes {
				createdAt
			  }
			}
		  }
		}
`
	simple := newQuery(url, query)
	simple.token = os.Getenv("GH_TOKEN")
	path := strings.Split("repository.issues.pageInfo", ".")
	pq := PaginatedQuery{
		simpleQuery:   simple,
		maxPages:      2,
		pagesPath:     path,
		cursorProp:    "endCursor",
		hasNextProp:   "hasNextPage",
		cursorVarName: "after",
	}
	if err := pq.execPaginated(); err != nil {
		t.Error(err)
	}
}

func TestPaginatedQueryWithVars(t *testing.T) {
	url := "https://api.github.com/graphql"
	query := `
		query($perPage:Int,$after:String) { 
		  repository(owner:"ethereum", name:"go-ethereum") {
			issues(first:$perPage, after:$after) {
			  pageInfo {
				endCursor
				hasNextPage
			  }
			  nodes {
				createdAt
			  }
			}
		  }
		}
`
	path := strings.Split("repository.issues", ".")
	pq := newPaginatedQuery(url, query, path)
	pq.maxPages = 2
	pq.simpleQuery.token = os.Getenv("GH_TOKEN")
	pq.simpleQuery.vars = map[string]interface{}{
		"perPage": 2,
	}
	if err := pq.execPaginated(); err != nil {
		t.Error(err)
	}
}
func TestGetProp(t *testing.T) {
	var obj = map[string]interface{}{
		"shallow": "here",
		"nilValue": nil,
		"nested" : map[string]interface{}{
			"foo": "bar",
		},
	}
	tests := []struct {
		path string
		wanted interface{}
	}{
		{
			path: "shallow",
			wanted: "here",
		},
		{
			path: "nilValue",
			wanted: nil,
		},
		{
			path: "missing.missing",
			wanted: nil,
		},
		{
			path: "nested.foo",
			wanted: "bar",
		},
	}
	for _, tc := range tests {
		path := strings.Split(tc.path, ".")
		if got := getProp(obj, path); got != tc.wanted {
			t.Errorf("Path %v, wanted: %v, got %v", path, tc.wanted, got)
		}

	}

}
